#!/bin/bash
rm -rf public/
mkdir public/
cp -r main_page/*.* public/
cp -r media/ public/
for file in slides/*.slides
do
    mdslides $file
    dirname=$(basename $file .slides)/
    # echo $dirname
    mv $dirname public/$dirname
done


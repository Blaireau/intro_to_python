[comment]: # (CODE_THEME = base16/material-darker) best dark theme in the list
[comment]: # (CODE_THEME = github) best light theme in the list
[comment]: # (THEME = white)

# Programming with python

## Repeating instructions

[comment]: # (!!!)

## For loop

We can repeat one or a group of instructions using a for loop.
```py
for loop in range(10):
    print("Hooray!")
```

We provide a `int` to the range function: the number of repetitions we want.

[comment]: # (!!!)

We use **indentation** to distinguish which instructions belong to the for loop.

```py
print('Starting countdown.')
count = 10
for loop in range(10):
    print(count) # will be repeated
    count = count - 1 # will also be repeated
print('Liftoff !') # will run only once
```

[comment]: # (!!!)

## Challenge

Make a programm that prints multiplication tables.

Usage example:

```txt
Multiplication table of: 5
0 x 5 = 0
1 x 5 = 5
2 x 5 = 10
3 x 5 = 15
4 x 5 = 20
5 x 5 = 25
6 x 5 = 30
7 x 5 = 35
8 x 5 = 40
9 x 5 = 45
10 x 5 = 50
```

[comment]: # (!!!)

## Challenge 2

- Year 0: There is a forest with a certain number of trees.

- Year 1: The first year, the forest grows and the number of trees increases by 20%.  
After that, the humans cut down a tree.

- Year 2: The forest grows again by 20%.  
The humans are greedier and cut down two trees.

- Year 3: The forest grows by 20%.  
The humans are cut down three trees.

[comment]: # (!!!)

- And so on, every year, the forest grows by 20% and the humans cut one more tree than the previous year.  

> How many trees in the forest in 50 years? And in a century?  
> How does the initial number of trees in the forest influence the result?  

[comment]: # (!!!)

### End of presentation

[Go back to main menu](../index.html)

# A little bit of everything about computers

A static website that contains different presentations about computer science, python programming and similar things.

[Goto the website](https://blaireau.frama.io/intro_to_python)

## Project structure

- `main_page`: the content of the main page of the website
- `slides`: the slides presentations source files ([markdown slides format](https://gitlab.com/da_doomer/markdown-slides))
- `media`: a folder to put all media files that needs to be included in the website

## Compilation

You need to have [mdslides](https://gitlab.com/da_doomer/markdown-slides) installed.

To build the website, run:

```
./build_all.sh
```



